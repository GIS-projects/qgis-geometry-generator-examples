# Chaos lines
If you prefer your maps chaotic, this is your style.  The example creates a curly line between the center of your screen and each point. So each time you pan the map, the lines will change.

## Geometry generator code
```
with_variable( 'start_x', x( @map_extent_center ), 
	with_variable( 'start_y',  y( @map_extent_center ), 
		smooth( make_line(
			@geometry, 
				make_point(
					4*randf(0,1)*(x(@geometry)-@start_x)/5+@start_x,
					4*randf(0,1)*(y(@geometry)-@start_y)/5+@start_y
				),
				make_point(
					3*randf(0,1)*(x(@geometry)-@start_x)/5+@start_x,
					3*randf(0,1)*(y(@geometry)-@start_y)/5+@start_y
				),
				make_point(
					2*randf(0,1)*(x(@geometry)-@start_x)/5+@start_x,
					2*randf(0,1)*(y(@geometry)-@start_y)/5+@start_y
				),
				make_point(
					1*randf(0,1)*(x(@geometry)-@start_x)/5+@start_x,
					1*randf(0,1)*(y(@geometry)-@start_y)/5+@start_y
				),
				make_point(
					@start_x, 
					@start_y
				)
			),
			5,0.3,0.1,360
		)
	)
)
```

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/chaos_lines/chaos_lines.qml?inline=false"><img src="../../Example_images/chaos_lines.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/chaos_lines/chaos_lines.qml?inline=false)

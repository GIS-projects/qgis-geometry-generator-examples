<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.32.0-Lima" styleCategories="Symbology">
  <renderer-v2 type="singleSymbol" forceraster="0" enableorderby="0" referencescale="-1" symbollevels="0">
    <symbols>
      <symbol type="marker" clip_to_extent="1" is_animated="0" force_rhr="0" name="0" frame_rate="10" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{836601ce-c951-47c1-8c24-04b64a24739a}" enabled="1" locked="0" class="SimpleMarker">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="166,6,22,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="circle" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="2" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer pass="0" id="{71d999e4-784c-4dfe-9403-be7e7da1ffa7}" enabled="1" locked="0" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Line" name="SymbolType"/>
            <Option type="QString" value="with_variable( 'start_x', x( @map_extent_center ), &#xd;&#xa;&#x9;with_variable( 'start_y',  y( @map_extent_center ), &#xd;&#xa;&#x9;&#x9;smooth( make_line(&#xd;&#xa;&#x9;&#x9;&#x9;@geometry, &#xd;&#xa;&#x9;&#x9;&#x9;&#x9;make_point(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;4*randf(0,1)*(x(@geometry)-@start_x)/5+@start_x,&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;4*randf(0,1)*(y(@geometry)-@start_y)/5+@start_y&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;make_point(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;3*randf(0,1)*(x(@geometry)-@start_x)/5+@start_x,&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;3*randf(0,1)*(y(@geometry)-@start_y)/5+@start_y&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;make_point(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;2*randf(0,1)*(x(@geometry)-@start_x)/5+@start_x,&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;2*randf(0,1)*(y(@geometry)-@start_y)/5+@start_y&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;make_point(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;1*randf(0,1)*(x(@geometry)-@start_x)/5+@start_x,&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;1*randf(0,1)*(y(@geometry)-@start_y)/5+@start_y&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;make_point(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;@start_x, &#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;@start_y&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;)&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;5,0.3,0.1,360&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;)&#xd;&#xa;)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol type="line" clip_to_extent="1" is_animated="0" force_rhr="0" name="@0@1" frame_rate="10" alpha="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" id="{da626505-39af-4324-b102-625e0eee8388}" enabled="1" locked="0" class="SimpleLine">
              <Option type="Map">
                <Option type="QString" value="0" name="align_dash_pattern"/>
                <Option type="QString" value="round" name="capstyle"/>
                <Option type="QString" value="5;2" name="customdash"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
                <Option type="QString" value="MM" name="customdash_unit"/>
                <Option type="QString" value="0" name="dash_pattern_offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
                <Option type="QString" value="0" name="draw_inside_polygon"/>
                <Option type="QString" value="round" name="joinstyle"/>
                <Option type="QString" value="35,35,35,255" name="line_color"/>
                <Option type="QString" value="solid" name="line_style"/>
                <Option type="QString" value="0.3" name="line_width"/>
                <Option type="QString" value="MM" name="line_width_unit"/>
                <Option type="QString" value="0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="0" name="ring_filter"/>
                <Option type="QString" value="0" name="trim_distance_end"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
                <Option type="QString" value="MM" name="trim_distance_end_unit"/>
                <Option type="QString" value="0" name="trim_distance_start"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
                <Option type="QString" value="MM" name="trim_distance_start_unit"/>
                <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
                <Option type="QString" value="0" name="use_custom_dash"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerGeometryType>0</layerGeometryType>
</qgis>

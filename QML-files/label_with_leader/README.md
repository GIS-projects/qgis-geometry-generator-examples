# Labels with a leader
Creating labels with a leader is very common in CAD programs, but in a GIS this isn't always possible.  In QGIS you can add these kind of lables using styles.  Of course this will give the best results if your layer doesn't contain too many items that are too close together. In the example below the field id is used to label the points.

Since version 3.10 QGIS labels with <a href="https://www.gislounge.com/how-to-add-leader-lines-to-labels-in-qgis-3-10/" target="_blank">leader lines are a core functionality</a>.  They look a little different than my leader lines, but it is certainly a very powerful and useful tool. 

## Geometry generator code
### For the arrow
```
project(@geometry, 70, radians(35))
```

### For the line
```
make_line(project(@geometry, 50, radians(35)),  project(@geometry, 145.8, radians(35)), project(@geometry, 300, radians(65)))
```

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/label_with_leader/label_with_leader.qml?inline=false"><img src="../../Example_images/label_with_leader.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/label_with_leader/label_with_leader.qml?inline=false)

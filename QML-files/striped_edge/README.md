# Striped edge

If you want your polygons to have a striped edge, you can use Geometry Generators to do so.  In this example I have added a striped edge outside of the polygon, so the result is bigger than the original polygon. This outside edge is filled with a Line Pattern Fill. The inside of the original polygon is also filled with a Line Pattern Fill, but in the other direction.

## Geometry generator code
```
difference(
	buffer(@geometry, 100),
    @geometry
)
```

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/striped_edge/striped_edge.qml?inline=false"><img src="../../Example_images/striped_edge.png"></a></td></tr></table>

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/striped_edge/striped_edge.qml?inline=false)

# Holiday Random
This style is holiday themed, but it's actually all about adding randomized styling to your layer.  The stars are located on the points of your layer, but they change color anytime you interact with the map (zoom, move, select,...). At the same time christmas trees are added to the map at random locations inside the bounding box of your points layer. Everytime you interact with the map the trees change location.  For each point in your layer, 3 trees are shown on the map.

## Geometry generator code
### The stars
This is a polygon with a simple fill, created by combining two triangles.  

```
 combine( 
	 make_regular_polygon( 
		$geometry, 
		make_point(
			$x+100,
			$y
		),
		3
	),
	make_regular_polygon( 
		$geometry, 
		make_point(
			$x-100,
			$y
		),
		3
	)
)
```

The color is randomized using the following expression in a data definded override:
```
color_rgb( rand(0,255),rand(0,255),rand(0,255)+ x(@canvas_cursor_point)*0 )
```

### The trees
Trees are created within the bounding box of the current layer. The trees are made from two triangles as simple markers.

```
 with_variable( 
	'y_max',
	y_max(
		layer_property(  @layer , 'extent')
		),    
		with_variable( 
			'x_max',
			x_max(
				layer_property( @layer, 'extent')				),    
				 with_variable( 
					'y_min',
					y_min(
						layer_property( @layer, 'extent')
						),    
						with_variable( 
							'x_min',
							x_min(
								layer_property( @layer, 'extent')
								),    
									collect( 
										make_point( rand(@x_min,@x_max),rand(@y_min,@y_max+ y(@canvas_cursor_point)*0 )),
										make_point( rand(@x_min,@x_max),rand(@y_min,@y_max+ y(@canvas_cursor_point)*0 ))
								   )
						)
				)
		)
)
```


<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/holiday_random/holiday_random.qml?inline=false"><img src="../../Example_images/holiday_random.gif"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/holiday_random/holiday_random.qml?inline=false)

# Points as buildings
In QGIS you can create graphic representations of points that are sized based on the value in one of the fields using Geometry Generator Styles.  Of course this will give the best result if your layer doesn't contain too many items that are close together. In the example below the field *value* is used to size the buildings.

## Geometry generator code
### The door of the buildings
This is a simple marker line.

```
  with_variable( 'height',30,
	make_line( 
			make_point( 
			$x+15,
			$y	+30		),
		make_point( 
			$x+15,
			($y)+30+  @height )
			)
		)
```

### The windows of the buildings
These are created using a simple marker line.

```
  with_variable( 'height',30,
	make_line( 
			make_point( 
			$x,
			$y+ @height *3
			),
		make_point( 
			$x,
			($y)+ "value"*@height - @height )
			)
		)
```

### The front of the building
This is a polygon with a simple fill.

```
   with_variable( 'width',30,make_polygon(   
		make_line( 
			make_point( $x-@width,$y),  
			make_point( $x+@width,$y),
			make_point( $x+@width,($y)+ "value"*@width ),
			make_point( $x-@width,($y)+ "value"*@width)
		)
	)
)
```

### The top of the building
This is a polygon with a simple fill.

```
   with_variable( 'width',30,make_polygon(   
		make_line( 
			make_point( $x+@width/2,($y)+@width+ "value"*@width),
		    make_point( $x+@width*2,($y)+@width+ "value"*@width ),
			make_point( $x+@width,($y)+ "value"*@width ),
			make_point( $x-@width,($y)+ "value"*@width)
		)
	)
)
```

### The side of the building
This is a polygon with a simple fill.

```
   with_variable( 'width',30,make_polygon(   
		make_line( 
			make_point( $x+@width*2,($y)+@width+ "value"*@width),
		    make_point( $x+@width*2,($y)+@width),
			make_point( $x+@width,$y),
			make_point( $x+@width,($y)+ "value"*@width)
		)
	)
)
```


<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/points_as_building/points_as_building.qml?inline=false"><img src="../../Example_images/points_as_building.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/points_as_building/points_as_building.qml?inline=false)

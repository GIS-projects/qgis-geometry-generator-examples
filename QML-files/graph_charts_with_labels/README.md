# Graph charts with labels
This style creates a bar chart with two bars for each point and adds a label with the value for each bar above the bar, similar like graph charts in spreadsheets.  The same result can also be more or less achieved without geometry generators, using the "Diagrams" functionality of QGIS. But that way it's more difficult to place the labels at a consistent distance from the bar chart. In the project file both versions can be compared.

Since the example point layer only has one column with example values a second bar is created as "*value/2+10*", but if you have layer with multiple value fields, you can use a second value directly as another field.

## Geometry generator code
### Value 1 as a number
The number is created on top of the chart by combining a geometry generator and a font marker with a data defined override on "Character(s)".
```
make_point(
	x(@geometry)+15,
	y(@geometry)+20+ "value" *10
)
```

### Value 1 as a chart
The chart is created as a rectangle using a geometry generator.
```
make_rectangle_3points( 
	@geometry, 
	make_point(x(@geometry)+30,y(@geometry)), 
	make_point(x(@geometry),y(@geometry)+ "value" *10)
)
```

### Value 2 as a number
The number is created on top of the chart by combining a geometry generator and a font marker with a data defined override on "Character(s)". As value *"value"/2+10* is used.
```
make_point(
	x(@geometry)+45,
	y(@geometry)+20+ ("value"/2+10 )*10
)
```

### Value 2 as a chart
The chart is created as a rectangle using a geometry generator. As value *"value"/2+10* is used.
```
make_rectangle_3points( 
	make_point(x(@geometry)+30,y(@geometry)), 
	make_point(x(@geometry)+60,y(@geometry)), 
	make_point(x(@geometry)+30,y(@geometry)+ ("value"/2+10) *10)
)
```

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/graph_charts_with_labels/graph_charts_with_labels.qml?inline=false"><img src="../../Example_images/graph_charts_with_labels.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/graph_charts_with_labels/graph_charts_with_labels.qml?inline=false)

# Dynamic distance lines
These styles generate distance lines on your map everytime you interact with the map window. Interacting in this case can be for example select an object, use the pan tool or zoom in or out. Everytime you do these kind of actions distance lines will be shown between all items in the layer and the point on the map you interacted on.  

Of course these kind of styles will work best for layers that don't contain too many items.  Since the styles are based on the parameter @canvas_cursor_point, the generated lines are temporary and will never be saved. They aren't even exported with the "Export Map to Image" function. 

As an alternative you could also use @canvas_cursor_point instead of @canvas_cursor_point.  In that case the dot and the end of the lays will always be in the center of the screen instead of where you clicked on the canvas.

## Geometry generator code
This style consists of a combination of three different geometry generators.  The length value is created as geometry generator as a substyle of another geometry generator.

### The dot that shows where in the map you clicked
This is a simple marker

```
@canvas_cursor_point
```

### The lines between the different map features and the point you clicked
This a Simple line

```
 shortest_line( $geometry, @canvas_cursor_point )
```

### The label that shows the length of each of those lines
This is a substyle on the previous geometry generator.  

```
centroid($geometry)
```
It is a font marker with a data defind override on the "Character(s)" with the following expression:

```
round(length( shortest_line( $geometry, @canvas_cursor_point )))||'m'
```

There is an expression used for the data defind override on the "Rotation".
```
degrees( azimuth( centroid($geometry), @canvas_cursor_point ))-90
```

<table><tr><td><img src="../../Example_images/dynamic_distance_lines.gif"></td></tr></table> 

[Download the QML file for this Geometry Generator Style for polygons](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dynamic_distance_lines/dynamic_distance_lines_for_polygons.qml?inline=false)

[Download the QML file for this Geometry Generator Style for lines](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dynamic_distance_lines/dynamic_distance_lines_for_lines.qml?inline=false)

[Download the QML file for this Geometry Generator Style for points](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dynamic_distance_lines/dynamic_distance_lines_for_points.qml?inline=false)

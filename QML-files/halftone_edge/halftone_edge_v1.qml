<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.32.0-Lima" styleCategories="Symbology">
  <renderer-v2 type="singleSymbol" forceraster="0" enableorderby="0" referencescale="-1" symbollevels="0">
    <symbols>
      <symbol type="fill" clip_to_extent="1" is_animated="0" force_rhr="0" name="0" frame_rate="10" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{a0227342-8400-41b4-9bb4-54d8a1d38836}" enabled="1" locked="0" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="smooth(@geometry,5)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol type="fill" clip_to_extent="1" is_animated="0" force_rhr="0" name="@0@0" frame_rate="10" alpha="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" id="{88fc9d18-24d6-4737-97b2-957bc962e483}" enabled="1" locked="0" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="114,155,111,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0.5" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="no" name="style"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
            <layer pass="0" id="{e295dd8f-1e33-4426-a649-719632444f9f}" enabled="1" locked="0" class="GeometryGenerator">
              <Option type="Map">
                <Option type="QString" value="Line" name="SymbolType"/>
                <Option type="QString" value="boundary(buffer($geometry,-25))" name="geometryModifier"/>
                <Option type="QString" value="MapUnit" name="units"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
              <symbol type="line" clip_to_extent="1" is_animated="0" force_rhr="0" name="@@0@0@1" frame_rate="10" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="{0e342862-687b-47b6-adcc-7ab2e735a2cc}" enabled="1" locked="0" class="MarkerLine">
                  <Option type="Map">
                    <Option type="QString" value="4" name="average_angle_length"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale"/>
                    <Option type="QString" value="MM" name="average_angle_unit"/>
                    <Option type="QString" value="50" name="interval"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="interval_map_unit_scale"/>
                    <Option type="QString" value="RenderMetersInMapUnits" name="interval_unit"/>
                    <Option type="QString" value="0" name="offset"/>
                    <Option type="QString" value="0" name="offset_along_line"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_along_line_unit"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="bool" value="true" name="place_on_every_part"/>
                    <Option type="QString" value="Interval" name="placements"/>
                    <Option type="QString" value="1" name="ring_filter"/>
                    <Option type="QString" value="0" name="rotate"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option type="Map" name="properties">
                        <Option type="Map" name="interval">
                          <Option type="bool" value="false" name="active"/>
                          <Option type="int" value="1" name="type"/>
                          <Option type="QString" value="" name="val"/>
                        </Option>
                      </Option>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                  <symbol type="marker" clip_to_extent="1" is_animated="0" force_rhr="0" name="@@@0@0@1@0" frame_rate="10" alpha="1">
                    <data_defined_properties>
                      <Option type="Map">
                        <Option type="QString" value="" name="name"/>
                        <Option name="properties"/>
                        <Option type="QString" value="collection" name="type"/>
                      </Option>
                    </data_defined_properties>
                    <layer pass="0" id="{1ee355be-fd2f-4943-a2af-1388d4c1d073}" enabled="1" locked="0" class="SimpleMarker">
                      <Option type="Map">
                        <Option type="QString" value="0" name="angle"/>
                        <Option type="QString" value="square" name="cap_style"/>
                        <Option type="QString" value="0,0,0,255" name="color"/>
                        <Option type="QString" value="1" name="horizontal_anchor_point"/>
                        <Option type="QString" value="bevel" name="joinstyle"/>
                        <Option type="QString" value="circle" name="name"/>
                        <Option type="QString" value="0,0" name="offset"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                        <Option type="QString" value="MM" name="offset_unit"/>
                        <Option type="QString" value="35,35,35,255" name="outline_color"/>
                        <Option type="QString" value="solid" name="outline_style"/>
                        <Option type="QString" value="0" name="outline_width"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                        <Option type="QString" value="MM" name="outline_width_unit"/>
                        <Option type="QString" value="diameter" name="scale_method"/>
                        <Option type="QString" value="50" name="size"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                        <Option type="QString" value="RenderMetersInMapUnits" name="size_unit"/>
                        <Option type="QString" value="1" name="vertical_anchor_point"/>
                      </Option>
                      <data_defined_properties>
                        <Option type="Map">
                          <Option type="QString" value="" name="name"/>
                          <Option name="properties"/>
                          <Option type="QString" value="collection" name="type"/>
                        </Option>
                      </data_defined_properties>
                    </layer>
                  </symbol>
                </layer>
              </symbol>
            </layer>
            <layer pass="0" id="{53391ea1-e970-4184-b453-1c99aa3026b0}" enabled="1" locked="0" class="GeometryGenerator">
              <Option type="Map">
                <Option type="QString" value="Line" name="SymbolType"/>
                <Option type="QString" value="boundary(buffer($geometry,-75))" name="geometryModifier"/>
                <Option type="QString" value="MapUnit" name="units"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
              <symbol type="line" clip_to_extent="1" is_animated="0" force_rhr="0" name="@@0@0@2" frame_rate="10" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="{daccc2b8-478d-41f7-80f9-eef2422b465b}" enabled="1" locked="0" class="MarkerLine">
                  <Option type="Map">
                    <Option type="QString" value="4" name="average_angle_length"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale"/>
                    <Option type="QString" value="MM" name="average_angle_unit"/>
                    <Option type="QString" value="50" name="interval"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="interval_map_unit_scale"/>
                    <Option type="QString" value="RenderMetersInMapUnits" name="interval_unit"/>
                    <Option type="QString" value="0" name="offset"/>
                    <Option type="QString" value="0" name="offset_along_line"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_along_line_unit"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="bool" value="true" name="place_on_every_part"/>
                    <Option type="QString" value="Interval" name="placements"/>
                    <Option type="QString" value="1" name="ring_filter"/>
                    <Option type="QString" value="0" name="rotate"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                  <symbol type="marker" clip_to_extent="1" is_animated="0" force_rhr="0" name="@@@0@0@2@0" frame_rate="10" alpha="1">
                    <data_defined_properties>
                      <Option type="Map">
                        <Option type="QString" value="" name="name"/>
                        <Option name="properties"/>
                        <Option type="QString" value="collection" name="type"/>
                      </Option>
                    </data_defined_properties>
                    <layer pass="0" id="{b49c16f8-a9fa-494f-a39c-ebc75cf436aa}" enabled="1" locked="0" class="SimpleMarker">
                      <Option type="Map">
                        <Option type="QString" value="0" name="angle"/>
                        <Option type="QString" value="square" name="cap_style"/>
                        <Option type="QString" value="0,0,0,255" name="color"/>
                        <Option type="QString" value="1" name="horizontal_anchor_point"/>
                        <Option type="QString" value="bevel" name="joinstyle"/>
                        <Option type="QString" value="circle" name="name"/>
                        <Option type="QString" value="0,0" name="offset"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                        <Option type="QString" value="MM" name="offset_unit"/>
                        <Option type="QString" value="35,35,35,255" name="outline_color"/>
                        <Option type="QString" value="solid" name="outline_style"/>
                        <Option type="QString" value="0" name="outline_width"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                        <Option type="QString" value="MM" name="outline_width_unit"/>
                        <Option type="QString" value="diameter" name="scale_method"/>
                        <Option type="QString" value="40" name="size"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                        <Option type="QString" value="RenderMetersInMapUnits" name="size_unit"/>
                        <Option type="QString" value="1" name="vertical_anchor_point"/>
                      </Option>
                      <data_defined_properties>
                        <Option type="Map">
                          <Option type="QString" value="" name="name"/>
                          <Option name="properties"/>
                          <Option type="QString" value="collection" name="type"/>
                        </Option>
                      </data_defined_properties>
                    </layer>
                  </symbol>
                </layer>
              </symbol>
            </layer>
            <layer pass="0" id="{f2cf7eca-b9f6-4fdf-8d58-554240a41e85}" enabled="1" locked="0" class="GeometryGenerator">
              <Option type="Map">
                <Option type="QString" value="Line" name="SymbolType"/>
                <Option type="QString" value="boundary(buffer($geometry,-125))" name="geometryModifier"/>
                <Option type="QString" value="MapUnit" name="units"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
              <symbol type="line" clip_to_extent="1" is_animated="0" force_rhr="0" name="@@0@0@3" frame_rate="10" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="{7fd1947e-c8c3-41a4-8bdf-2cb32e9b6521}" enabled="1" locked="0" class="MarkerLine">
                  <Option type="Map">
                    <Option type="QString" value="4" name="average_angle_length"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale"/>
                    <Option type="QString" value="MM" name="average_angle_unit"/>
                    <Option type="QString" value="50" name="interval"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="interval_map_unit_scale"/>
                    <Option type="QString" value="RenderMetersInMapUnits" name="interval_unit"/>
                    <Option type="QString" value="0" name="offset"/>
                    <Option type="QString" value="0" name="offset_along_line"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_along_line_unit"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="bool" value="true" name="place_on_every_part"/>
                    <Option type="QString" value="Interval" name="placements"/>
                    <Option type="QString" value="1" name="ring_filter"/>
                    <Option type="QString" value="0" name="rotate"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                  <symbol type="marker" clip_to_extent="1" is_animated="0" force_rhr="0" name="@@@0@0@3@0" frame_rate="10" alpha="1">
                    <data_defined_properties>
                      <Option type="Map">
                        <Option type="QString" value="" name="name"/>
                        <Option name="properties"/>
                        <Option type="QString" value="collection" name="type"/>
                      </Option>
                    </data_defined_properties>
                    <layer pass="0" id="{e5b0d7e4-492e-45a1-9843-1b54d6a77a14}" enabled="1" locked="0" class="SimpleMarker">
                      <Option type="Map">
                        <Option type="QString" value="0" name="angle"/>
                        <Option type="QString" value="square" name="cap_style"/>
                        <Option type="QString" value="0,0,0,255" name="color"/>
                        <Option type="QString" value="1" name="horizontal_anchor_point"/>
                        <Option type="QString" value="bevel" name="joinstyle"/>
                        <Option type="QString" value="circle" name="name"/>
                        <Option type="QString" value="0,0" name="offset"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                        <Option type="QString" value="MM" name="offset_unit"/>
                        <Option type="QString" value="35,35,35,255" name="outline_color"/>
                        <Option type="QString" value="solid" name="outline_style"/>
                        <Option type="QString" value="0" name="outline_width"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                        <Option type="QString" value="MM" name="outline_width_unit"/>
                        <Option type="QString" value="diameter" name="scale_method"/>
                        <Option type="QString" value="30" name="size"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                        <Option type="QString" value="RenderMetersInMapUnits" name="size_unit"/>
                        <Option type="QString" value="1" name="vertical_anchor_point"/>
                      </Option>
                      <data_defined_properties>
                        <Option type="Map">
                          <Option type="QString" value="" name="name"/>
                          <Option name="properties"/>
                          <Option type="QString" value="collection" name="type"/>
                        </Option>
                      </data_defined_properties>
                    </layer>
                  </symbol>
                </layer>
              </symbol>
            </layer>
            <layer pass="0" id="{d8741568-39fb-4198-b753-18a0ba478b52}" enabled="1" locked="0" class="GeometryGenerator">
              <Option type="Map">
                <Option type="QString" value="Line" name="SymbolType"/>
                <Option type="QString" value="boundary(buffer($geometry,-175))" name="geometryModifier"/>
                <Option type="QString" value="MapUnit" name="units"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
              <symbol type="line" clip_to_extent="1" is_animated="0" force_rhr="0" name="@@0@0@4" frame_rate="10" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="{a5d1868d-4158-4817-8661-e3dd4ec94dc3}" enabled="1" locked="0" class="MarkerLine">
                  <Option type="Map">
                    <Option type="QString" value="4" name="average_angle_length"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale"/>
                    <Option type="QString" value="MM" name="average_angle_unit"/>
                    <Option type="QString" value="50" name="interval"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="interval_map_unit_scale"/>
                    <Option type="QString" value="RenderMetersInMapUnits" name="interval_unit"/>
                    <Option type="QString" value="0" name="offset"/>
                    <Option type="QString" value="0" name="offset_along_line"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_along_line_unit"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="bool" value="true" name="place_on_every_part"/>
                    <Option type="QString" value="Interval" name="placements"/>
                    <Option type="QString" value="1" name="ring_filter"/>
                    <Option type="QString" value="0" name="rotate"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                  <symbol type="marker" clip_to_extent="1" is_animated="0" force_rhr="0" name="@@@0@0@4@0" frame_rate="10" alpha="1">
                    <data_defined_properties>
                      <Option type="Map">
                        <Option type="QString" value="" name="name"/>
                        <Option name="properties"/>
                        <Option type="QString" value="collection" name="type"/>
                      </Option>
                    </data_defined_properties>
                    <layer pass="0" id="{f018d1f4-a7df-4b65-aadf-6e10a57b7bd8}" enabled="1" locked="0" class="SimpleMarker">
                      <Option type="Map">
                        <Option type="QString" value="0" name="angle"/>
                        <Option type="QString" value="square" name="cap_style"/>
                        <Option type="QString" value="0,0,0,255" name="color"/>
                        <Option type="QString" value="1" name="horizontal_anchor_point"/>
                        <Option type="QString" value="bevel" name="joinstyle"/>
                        <Option type="QString" value="circle" name="name"/>
                        <Option type="QString" value="0,0" name="offset"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                        <Option type="QString" value="MM" name="offset_unit"/>
                        <Option type="QString" value="35,35,35,255" name="outline_color"/>
                        <Option type="QString" value="solid" name="outline_style"/>
                        <Option type="QString" value="0" name="outline_width"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                        <Option type="QString" value="MM" name="outline_width_unit"/>
                        <Option type="QString" value="diameter" name="scale_method"/>
                        <Option type="QString" value="20" name="size"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                        <Option type="QString" value="RenderMetersInMapUnits" name="size_unit"/>
                        <Option type="QString" value="1" name="vertical_anchor_point"/>
                      </Option>
                      <data_defined_properties>
                        <Option type="Map">
                          <Option type="QString" value="" name="name"/>
                          <Option name="properties"/>
                          <Option type="QString" value="collection" name="type"/>
                        </Option>
                      </data_defined_properties>
                    </layer>
                  </symbol>
                </layer>
              </symbol>
            </layer>
            <layer pass="0" id="{ef7b5fc3-69b9-4fa0-af49-9b134c200129}" enabled="1" locked="0" class="GeometryGenerator">
              <Option type="Map">
                <Option type="QString" value="Line" name="SymbolType"/>
                <Option type="QString" value="boundary(buffer($geometry,-225))" name="geometryModifier"/>
                <Option type="QString" value="MapUnit" name="units"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
              <symbol type="line" clip_to_extent="1" is_animated="0" force_rhr="0" name="@@0@0@5" frame_rate="10" alpha="1">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="{533becfc-5e76-45fe-a958-678a042511e4}" enabled="1" locked="0" class="MarkerLine">
                  <Option type="Map">
                    <Option type="QString" value="4" name="average_angle_length"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale"/>
                    <Option type="QString" value="MM" name="average_angle_unit"/>
                    <Option type="QString" value="50" name="interval"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="interval_map_unit_scale"/>
                    <Option type="QString" value="RenderMetersInMapUnits" name="interval_unit"/>
                    <Option type="QString" value="0" name="offset"/>
                    <Option type="QString" value="0" name="offset_along_line"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_along_line_unit"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="bool" value="true" name="place_on_every_part"/>
                    <Option type="QString" value="Interval" name="placements"/>
                    <Option type="QString" value="1" name="ring_filter"/>
                    <Option type="QString" value="0" name="rotate"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                  <symbol type="marker" clip_to_extent="1" is_animated="0" force_rhr="0" name="@@@0@0@5@0" frame_rate="10" alpha="1">
                    <data_defined_properties>
                      <Option type="Map">
                        <Option type="QString" value="" name="name"/>
                        <Option name="properties"/>
                        <Option type="QString" value="collection" name="type"/>
                      </Option>
                    </data_defined_properties>
                    <layer pass="0" id="{65e0aaca-dc9e-4858-b17c-811f1d8ded58}" enabled="1" locked="0" class="SimpleMarker">
                      <Option type="Map">
                        <Option type="QString" value="0" name="angle"/>
                        <Option type="QString" value="square" name="cap_style"/>
                        <Option type="QString" value="0,0,0,255" name="color"/>
                        <Option type="QString" value="1" name="horizontal_anchor_point"/>
                        <Option type="QString" value="bevel" name="joinstyle"/>
                        <Option type="QString" value="circle" name="name"/>
                        <Option type="QString" value="0,0" name="offset"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                        <Option type="QString" value="MM" name="offset_unit"/>
                        <Option type="QString" value="35,35,35,255" name="outline_color"/>
                        <Option type="QString" value="solid" name="outline_style"/>
                        <Option type="QString" value="0" name="outline_width"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                        <Option type="QString" value="MM" name="outline_width_unit"/>
                        <Option type="QString" value="diameter" name="scale_method"/>
                        <Option type="QString" value="10" name="size"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                        <Option type="QString" value="RenderMetersInMapUnits" name="size_unit"/>
                        <Option type="QString" value="1" name="vertical_anchor_point"/>
                      </Option>
                      <data_defined_properties>
                        <Option type="Map">
                          <Option type="QString" value="" name="name"/>
                          <Option name="properties"/>
                          <Option type="QString" value="collection" name="type"/>
                        </Option>
                      </data_defined_properties>
                    </layer>
                  </symbol>
                </layer>
              </symbol>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerGeometryType>2</layerGeometryType>
</qgis>

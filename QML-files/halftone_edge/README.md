# Halftone edge
With this style you can add points to the inside of the edge of polygons that resemble a halftone pattern. Version 1 of this style uses Geometry Generators multiple times to achieve the result.  version 2 of the style uses only one Geometry Generator and uses Line Offset to achieve a similar result.  The results of both versions are slightly different, so you use the version that suits your layer best.

## Geometry generator code
### Version 1: Multiple geometry generators
#### Smoothing the polygons
Because a halftone edge gives the best result in rounded corners, we smooth the polygons. We use a geometry generator as the parent style and all the other geometry generators in this style will use the result of this as the basis to work on. That is the reason we use @geometry in this geometry generator and $geometry in all the other.

```
smooth(@geometry,5)
```

#### Creating lines of Dots
We use multiple geometry generators with different negative buffers to create Marker lines to create the dots.

```
boundary(buffer($geometry,-225))
```
```
boundary(buffer($geometry,-175))
```
```
boundary(buffer($geometry,-125))
```
```
boundary(buffer($geometry,-75))
```
```
boundary(buffer($geometry,-25))
```

### Version 2: Just one geometry generator + line offset
#### Smoothing the polygons
Because a halftone edge gives the best result in rounded corners, we smooth the polygons. We use a geometry generator as the parent style and all the other items in this style will use the result of this as the basis to work on. 

```
smooth(@geometry,5)
```

#### Creating lines of Dots
The dots are created using 5 substyles that are just Marker lines with each a different offset.  This shows you can get the same result with different methods.

<table><tr><td><img src="../../Example_images/halftone_edge.png"></td></tr></table> 

[Download the QML file for this Geometry Generator Style version 1](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/halftone_edge/halftone_edge_v1.qml?inline=false)

[Download the QML file for this Geometry Generator Style version 2](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/halftone_edge/halftone_edge_v2.qml?inline=false)

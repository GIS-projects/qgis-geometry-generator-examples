# Numbered nodes
If you have a polyline layer and you want to emphasize the nodes of your polylines, you can number them.

## Geometry generator code
```
nodes_to_points( $geometry)
```

The following expression is used as a data defined override on the "Character(s)"
```
@geometry_part_num
```


<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/numbered_nodes/numbered_nodes.qml?inline=false"><img src="../../Example_images/numbered_nodes.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/numbered_nodes/numbered_nodes.qml?inline=false)

# Random points
In QGIS you can add random points to the inside of the edge of polygons. First I used pseudo-random points using a combination of Geometry generators and Point pattern fills in styles, but this uses a lot of memory and sometimes makes QGIS crash.  Later I created a real random points version that uses a combination of a Geometry Generator and a simple marker line with an expression for the Offset setting and that is super fast to use.

## Geometry generator code
### Real random points
```
force_rhr(@geometry)
```

The following expression is used in the Offset setting of the simple marker of the marker line:
```
concat('0,',10*randf(0,1) ^ 3)
```

### Pseudo random points
This geometry code is set up in three parts:

```
sym_difference( 
	@geometry, 
	buffer( 
		buffer( 
			@geometry,
			-150),
		80)
	
```

```
sym_difference( 
	@geometry, 
	buffer( 
		buffer( 
			@geometry,
			-250),
		100)
	
```

```
sym_difference( 
	@geometry, 
	buffer( 
		buffer( 
			@geometry,
			-300),
		100)
	
```

**real random points version**
<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/random_points_edge/real-random_points_edge.qml?inline=false"><img src="../../Example_images/real-random_points_edge.png"></a></td></tr></table>

**pseudo random points version**
<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/random_points_edge/pseudo-random_points_edge.qml?inline=false"><img src="../../Example_images/pseudo-random_points_edge.png"></a></td></tr></table>  




[Download the QML file for the real random Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/random_points_edge/real-random_points_edge.qml?inline=false)

[Download the QML file for the pseudo random Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/random_points_edge/pseudo-random_points_edge.qml?inline=false)

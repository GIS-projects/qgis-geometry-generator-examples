# Gradient border
This simple QGIS style changes the border of polygons to a gradient. It uses a negative buffer to create the border, so this means the outside of the polygon remains the same.  You could also make a double sided buffer, but that would change the outline of the polygon.  I also created an animated version for QGIS 3.26 and later. 

Of course you can make a similar style for polyline layers using the same technique.

## Geometry generator code
The geometry generator is used to create a polygon of 50 meters on the inside of the source polygons. To create the gradient we use a "Gradient Fill" style.

```
difference( 
	@geometry, 
	buffer(@geometry, -50)
)
```

To create the animation first go to to the "Fill" style settings and press the "Advanced" button (that is below the predefined style symbols). There you select "Animation Settings".  

![Animation settings](animation_settings.png){: .shadow}

In the settings dialog that opens you check the "Is Animated" checkbox and set the Frame Rate to 120fps.

After this is set, a data defined override is used on the Rotation of the Gradient fill with the following expression:
```
@symbol_frame % 360
```

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gradient_border/gradient_border.qml?inline=false"><img src="../../Example_images/gradient_border.png"></a></td></tr>
</table>

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gradient_border/gradient_border.qml?inline=false)

<table>
<tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gradient_border/gradient_border_animated.qml?inline=false"><img src="../../Example_images/gradient_border_animated.gif"></a></td></tr></table> 

[Download the QML file for the animated Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gradient_border/gradient_border_animated.qml?inline=false)

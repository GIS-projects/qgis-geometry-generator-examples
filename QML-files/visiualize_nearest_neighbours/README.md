# Visiualize nearest neighbours
This QGIS style will draw a curved line between a point and the closest other point in that same layer. This way you can visualize nearest neighbours and also see clusters of points.

## Geometry generator code
This style is created with a geometry generator as a substyle of another geometry generator.

### This geometry generator is used to create a line between the current point and its closest neighbour
```
make_line( 
	@geometry,
	closest_point( 
		difference(
			aggregate( 
				@layer,
				'collect',
				@geometry),
			@geometry),
		@geometry
	)
)
```
### The substyle to make it curved
This is an arrow line that is made thicker on one side. The setting "curved arrows" is checked.

```
 make_line(
	 start_point($geometry),
	 centroid(
		offset_curve( 
			make_line(
				start_point($geometry),
				end_point($geometry)),
				length(
					make_line(
						start_point($geometry),
						end_point($geometry)
						)
				)/8
		 )
	),
	end_point($geometry)
)
```

In this case $geometry is used instead of @geometry, because we want to apply the geometry generator on the result of the parent geometry generator instead of on the original point.

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/visiualize_nearest_neighbours/visiualize_nearest_neighbours.qml?inline=false"><img src="../../Example_images/visiualize_nearest_neighbours.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/visiualize_nearest_neighbours/visiualize_nearest_neighbours.qml?inline=false)

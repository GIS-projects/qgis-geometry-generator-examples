<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="Symbology" version="3.32.0-Lima">
  <renderer-v2 type="singleSymbol" forceraster="0" referencescale="-1" enableorderby="0" symbollevels="0">
    <symbols>
      <symbol type="marker" alpha="1" is_animated="0" force_rhr="0" frame_rate="10" clip_to_extent="1" name="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" class="GeometryGenerator" pass="0" id="{ef6d248f-8ce4-440f-8b04-0a90fe0d85d7}">
          <Option type="Map">
            <Option type="QString" value="Line" name="SymbolType"/>
            <Option type="QString" value="make_line( &#xd;&#xa;&#x9;@geometry,&#xd;&#xa;&#x9;closest_point( &#xd;&#xa;&#x9;&#x9;difference(&#xd;&#xa;&#x9;&#x9;&#x9;aggregate( &#xd;&#xa;&#x9;&#x9;&#x9;&#x9;@layer,&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;'collect',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;@geometry),&#xd;&#xa;&#x9;&#x9;&#x9;@geometry),&#xd;&#xa;&#x9;&#x9;@geometry&#xd;&#xa;&#x9;)&#xd;&#xa;)&#xd;&#xa;" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol type="line" alpha="1" is_animated="0" force_rhr="0" frame_rate="10" clip_to_extent="1" name="@0@0">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer enabled="1" locked="0" class="GeometryGenerator" pass="0" id="{44755ba0-24b3-4839-b513-f77ca3675518}">
              <Option type="Map">
                <Option type="QString" value="Line" name="SymbolType"/>
                <Option type="QString" value=" make_line(&#xd;&#xa;&#x9; start_point($geometry),&#xd;&#xa;&#x9; centroid(&#xd;&#xa;&#x9;&#x9;offset_curve( &#xd;&#xa;&#x9;&#x9;&#x9;make_line(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;start_point($geometry),&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;end_point($geometry)),&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;length(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;make_line(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;start_point($geometry),&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;end_point($geometry)&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;)&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;)/8&#xd;&#xa;&#x9;&#x9; )&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;end_point($geometry)&#xd;&#xa;)" name="geometryModifier"/>
                <Option type="QString" value="MapUnit" name="units"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
              <symbol type="line" alpha="1" is_animated="0" force_rhr="0" frame_rate="10" clip_to_extent="1" name="@@0@0@0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer enabled="1" locked="0" class="ArrowLine" pass="0" id="{f0b8d87f-c563-449c-9e8b-6efea7f1d23c}">
                  <Option type="Map">
                    <Option type="QString" value="0.8" name="arrow_start_width"/>
                    <Option type="QString" value="MM" name="arrow_start_width_unit"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="arrow_start_width_unit_scale"/>
                    <Option type="QString" value="0" name="arrow_type"/>
                    <Option type="QString" value="0.1" name="arrow_width"/>
                    <Option type="QString" value="MM" name="arrow_width_unit"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="arrow_width_unit_scale"/>
                    <Option type="QString" value="0" name="head_length"/>
                    <Option type="QString" value="MM" name="head_length_unit"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="head_length_unit_scale"/>
                    <Option type="QString" value="0" name="head_thickness"/>
                    <Option type="QString" value="MM" name="head_thickness_unit"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="head_thickness_unit_scale"/>
                    <Option type="QString" value="0" name="head_type"/>
                    <Option type="QString" value="1" name="is_curved"/>
                    <Option type="QString" value="0" name="is_repeated"/>
                    <Option type="QString" value="0" name="offset"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_unit_scale"/>
                    <Option type="QString" value="0" name="ring_filter"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                  <symbol type="fill" alpha="1" is_animated="0" force_rhr="0" frame_rate="10" clip_to_extent="1" name="@@@0@0@0@0">
                    <data_defined_properties>
                      <Option type="Map">
                        <Option type="QString" value="" name="name"/>
                        <Option name="properties"/>
                        <Option type="QString" value="collection" name="type"/>
                      </Option>
                    </data_defined_properties>
                    <layer enabled="1" locked="0" class="SimpleFill" pass="0" id="{08924907-0307-4b8e-9de2-6b0c1357ea32}">
                      <Option type="Map">
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                        <Option type="QString" value="79,79,79,255" name="color"/>
                        <Option type="QString" value="bevel" name="joinstyle"/>
                        <Option type="QString" value="0,0" name="offset"/>
                        <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                        <Option type="QString" value="MM" name="offset_unit"/>
                        <Option type="QString" value="35,35,35,255" name="outline_color"/>
                        <Option type="QString" value="no" name="outline_style"/>
                        <Option type="QString" value="0.26" name="outline_width"/>
                        <Option type="QString" value="MM" name="outline_width_unit"/>
                        <Option type="QString" value="solid" name="style"/>
                      </Option>
                      <data_defined_properties>
                        <Option type="Map">
                          <Option type="QString" value="" name="name"/>
                          <Option name="properties"/>
                          <Option type="QString" value="collection" name="type"/>
                        </Option>
                      </data_defined_properties>
                    </layer>
                  </symbol>
                </layer>
              </symbol>
            </layer>
          </symbol>
        </layer>
        <layer enabled="1" locked="0" class="SimpleMarker" pass="0" id="{d0fcc1cd-d816-4b02-880c-a5d85fa25ccd}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="255,0,0,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="circle" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="2" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <blendMode>0</blendMode>
  <featureBlendMode>6</featureBlendMode>
  <layerGeometryType>0</layerGeometryType>
</qgis>

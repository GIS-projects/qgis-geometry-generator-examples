# Adding dimensions
Adding dimensions is very common in CAD programs, but in a GIS this usually isn't possible.  In QGIS you can add dimensions to polygons or lines using styles.  Of course this will give the best results if your layer doesn't contain too complex items.  Below you can find a styling for polygon layers and one for line layers. 

This style is a small geometry generator code that is combined with a marker line with a font marker, a marker line with simple markers and an arrow symbol style.

Because [a bug in QGIS](https://issues.qgis.org/issues/18384) was solved in QGIS 3.6, it's easier to create this style in QGIS since version 3.6 than in previous versions. Therefore per layer type two different versions of the QML-files are available to download.

## Geometry generator code
```
segments_to_lines($geometry)
```

## Expression to avoid upside down labels
This code is used in the rotation setting of the font marker.

```
CASE WHEN azimuth(
		start_point( geometry_n( $geometry, @geometry_part_num)),
		end_point( geometry_n( $geometry, @geometry_part_num))
	)> pi()  THEN degrees(azimuth(
		start_point( geometry_n( $geometry, @geometry_part_num)),
		end_point( geometry_n( $geometry, @geometry_part_num))
	)) + 90 + @map_rotation
	ELSE
	degrees(azimuth(
		start_point( geometry_n( $geometry, @geometry_part_num)),
		end_point( geometry_n( $geometry, @geometry_part_num))
	)) - 90 + @map_rotation
	END
```

## Dimensions for Polygons
<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dimensions/dimensions_polygon-qgis3-6.qml?inline=false"><img src="../../Example_images/dimensions_polygon.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style for QGIS 3.6 and newer versions](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dimensions/dimensions_polygon-qgis3-6.qml?inline=false)

[Download the QML file for this Geometry Generator Style for QGIS 3.0, 3.2 & 3.4](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dimensions/dimensions_polygon-qgis3-0_3-4.qml?inline=false)

## Dimensions for Lines
<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dimensions/dimensions_line-qgis3-6.qml?inline=false"><img src="../../Example_images/dimensions_line.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style for QGIS 3.6 and newer versions](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dimensions/dimensions_line-qgis3-6.qml?inline=false)

[Download the QML file for this Geometry Generator Style for QGIS 3.0, 3.2 & 3.4](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dimensions/dimensions_line-qgis3-0_3-4.qml?inline=false)
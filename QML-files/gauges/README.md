# Gauges
This style places a gauge on each polygon that shows an indication of the value in the table.  The exact value is also shown below the gauge.

## Geometry generator code
This style is a little more complex because it uses multiple geometry generators where some cover parts of others to get the result we want. Check out the example project file to see how the result is constructed.

### Polygon to point
The polygon is converted to a point.  We use point_on_surface() to make sure the point is within the polygon. All the following geometry generators are substyles of this one.  For that reason all other geometry generators in this style will use $geometry instead of @geometry, because we want to use the resulting point instead of the original polygon.

```
point_on_surface(@geometry)
```

### The value field as a number
This is actually not a geometry generator but a normal font marker. We use a data defined override on "Character(s)" to show the value of the "value" field

### The background of the value number
To create a rounded rectangle we create a buffer of a line with a geometry generator.

```
buffer(
	make_line(  
		make_point(
			x($geometry)-100,
			y($geometry)+50
		),
		make_point(
			x($geometry)+100,
			y($geometry)+50)
		)
	, 90
)
```

### The pointer of the gauge
This is made using a Simple marker of a line. It uses a data defined override for the rotation with the following expression:

```
"value" * (360 / (maximum("value") / 0.9))
```

The 0.9 in the expression is used to use only 90% of the 360° part of the green-to-red-indicator. Because below we will cover the other 10% with a grey triangle.

### The zero on the gauge
This is created using a font marker.

### The maximum value on the gauge
This is created using a font marker. We use a data defined override on "Character(s)" to show the maximum value of the "value" field. 
```
maximum("value")
```

### The small stripe to indicate the zero value.
This is created using a simple marker.

### The central black dot
This is created using a simple marker.

### A grey triangle to cover a part of the gauge between the maximum and the minumum
This is a grey triangle that covers a small part at the top of the green-to-red value indicator (10% is covered, see above for more info).  It is used to avoid that minimum and maximum colors touch each other directly. The pointer that was created above will use the most red part of that value indicator that is visible as the maximum possible value (see image below).

It is created using a geometry generator:

```
 make_triangle( 
	$geometry, 
	make_point(x($geometry)-280,y($geometry)-380), 
	make_point(x($geometry),y($geometry)-410)
	)
```

### Grey circle to cover central part of green-to-red value indicator
The green-to-red value indicator is actually a circle.  to make it into a circular line, we cover the central part with a grey circle.  It is created using a simple marker.

### The green-to-red value indicator
This is created as a circle (created as buffer around our central point) with a gradient fill.  The geometry generator that is used is:

```
buffer($geometry,400,20)
```

### The grey background circle
This is created using a simple marker.


<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gauges/gauges.qml?inline=false"><img src="../../Example_images/gauges.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gauges/gauges.qml?inline=false)

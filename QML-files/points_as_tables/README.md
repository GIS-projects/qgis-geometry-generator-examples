# Points as tables
If you want to create small tables on your map filled with values from the fields of a points table, you can use Geometry Generators to produce them.

## Geometry generator code
This style consists of a combination of eight different geometry generators.  It might look quite complex, but each of them is actually really simple.

### The number of the project at the op of the table
This is a font marker with a Data defined override on "Character(s)"

```
make_point($x, $y+75)
```

### The top right value
This is a font marker with a Data defined override on "Character(s)"

```
make_point($x+50, $y+15)
```

### The top left value
This is a font marker with a Data defined override on "Character(s)"

```
make_point($x-50, $y+15)
```

### The bottom left value
This is a font marker with a Data defined override on "Character(s)"

```
make_point($x-50, $y-60)
```

### The bottom right value
This is a font marker with a Data defined override on "Character(s)"

```
make_point($x+50, $y-60)
```

### The vertical line between the values
This is a simple line

```
make_line(
	make_point($x, $y-100),
	make_point($x, $y+50)
	)
```

### The outside box of the table
This is a rectangular polygon 

```
 make_regular_polygon( 
	$geometry, 
	make_point(
		$x+100,
		$y+100),
	4)
```

### The middle two vertical lines
This is a rectangular polygon 

```
 make_polygon( 
	 make_line( 
		make_point($x-100,$y+50),
		make_point($x+100,$y+50),
	    make_point($x+100,$y-25),
		make_point($x-100,$y-25),
		make_point($x-100,$y+50)
		)
	)
```



<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/points_as_tables/points_as_tables.qml?inline=false"><img src="../../Example_images/points_as_tables.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/points_as_tables/points_as_tables.qml?inline=false)

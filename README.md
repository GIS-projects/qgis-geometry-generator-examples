# QGIS Geometry Generator examples

I (Michel Stuyts) use **Geometry Generator** a lot in [QGIS](https://qgis.org). I promised to make some of my use cases available online: https://twitter.com/michelstuyts/status/1057049855489646592, so here they are. A great introduction to QGIS Geometry Generator by [Klas Karlsson](https://twitter.com/klaskarlsson) can be found on Youtube: https://www.youtube.com/watch?v=0YxjJ-9zIJ0. This repository contains some of my use cases of Geometry Generators. Often Geometry Generators are combined with Data Defined Overrides on certain settings.

All examples in this repository were created in **QGIS 3.x**  with both the project and the layers set to the Lambert 72 Belgium CRS ([EPSG:31370](https://epsg.io/31370)), that has meters as units. In the style settings the units to set sizes "Map Units" and "Meters at scale" are frequently used, so in EPSG:31370 both will result in Meters.

All examples have been tested and updated to work with QGIS 3.32.  Since the possibilities of expressions change sometimes between QGIS versions, the expressions may need small adjustments in other versions of QGIS.  If you find any problems with one of the styles, please [file a bug report](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/-/issues).

Below you will find an example of all styles currently in this repository.  This repository contains all **project files**, **QML files** and **sample data** to try the styles yourself. You can also download this entire repository at once: https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/-/archive/master/qgis-geometry-generator-examples-master.zip

## Index of examples
- [QGIS Geometry Generator examples](#qgis-geometry-generator-examples)
  - [Index of examples](#index-of-examples)
  - [List of examples](#list-of-examples)
    - [Dimensions for Polygons](#dimensions-for-polygons)
    - [Dimensions for Lines](#dimensions-for-lines)
    - [Direction](#direction)
    - [One arrow per line of a polyline](#one-arrow-per-line-of-a-polyline)
    - [Labels with a leader](#labels-with-a-leader)
    - [Random points on an edge](#random-points-on-an-edge)
    - [Points as buildings](#points-as-buildings)
    - [Obscurify privacy sensitive data](#obscurify-privacy-sensitive-data)
    - [Mixed orthogonal stripes at an edge](#mixed-orthogonal-stripes-at-an-edge)
    - [Points as tables](#points-as-tables)
    - [Dynamic distance lines](#dynamic-distance-lines)
    - [Holiday Random](#holiday-random)
    - [Building Footprint Poster](#building-footprint-poster)
    - [Visiualize nearest neighbours](#visiualize-nearest-neighbours)
    - [Numbered nodes](#numbered-nodes)
    - [Gauges](#gauges)
    - [Halftone edge](#halftone-edge)
    - [Graph charts with labels](#graph-charts-with-labels)
    - [Chaos lines](#chaos-lines)
    - [Bevelled edge](#bevelled-edge)
    - [Striped edge](#striped-edge)
    - [Gradient border](#gradient-border)

## List of examples
### Dimensions for Polygons
<table><tr><td><a href="QML-files/dimensions/"><img src="Example_images/dimensions_polygon.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/dimensions/)

### Dimensions for Lines
<table><tr><td><a href="QML-files/dimensions/"><img src="Example_images/dimensions_line.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/dimensions/)

### Direction 
##### One arrow per line of a polyline
<table><tr><td><a href="QML-files/direction/"><img src="Example_images/direction.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/direction/)

### Labels with a leader 
<table><tr><td><a href="QML-files/label_with_leader/"><img src="Example_images/label_with_leader.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/label_with_leader/)

### Random points on an edge
<table><tr><td><a href="QML-files/random_points_edge/"><img src="Example_images/real-random_points_edge.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/random_points_edge/)

### Points as buildings
<table><tr><td><a href="QML-files/points_as_building/"><img src="Example_images/points_as_building.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/points_as_building/)

### Obscurify privacy sensitive data
<table><tr><td><a href="QML-files/obscurify_privacy_sensitive_data/"><img src="Example_images/obscurify_privacy_sensitive_data.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/obscurify_privacy_sensitive_data/)

### Mixed orthogonal stripes at an edge
<table><tr><td><a href="QML-files/mixed_orthogonal_stripes_at_an_edge/"><img src="Example_images/mixed_orthogonal_stripes_at_an_edge.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/mixed_orthogonal_stripes_at_an_edge/)

### Points as tables
<table><tr><td><a href="QML-files/points_as_tables/"><img src="Example_images/points_as_tables.png" loading="eager"></a></td></tr></table> 
[Details of this Geometry Generator Style](QML-files/points_as_tables/)


### Dynamic distance lines
<table><tr><td><a href="QML-files/dynamic_distance_lines/"><img src="Example_images/dynamic_distance_lines.gif" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/dynamic_distance_lines/)

### Holiday Random
<table><tr><td><a href="QML-files/holiday_random/"><img src="Example_images/holiday_random.gif" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/holiday_random/)

### Building Footprint Poster
<table><tr><td><a href="QML-files/building_footprint_poster/"><img src="Example_images/building_footprint_poster.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/building_footprint_poster/)

### Visiualize nearest neighbours
<table><tr><td><a href="QML-files/visiualize_nearest_neighbours/"><img src="Example_images/visiualize_nearest_neighbours.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/visiualize_nearest_neighbours/)

### Numbered nodes
<table><tr><td><a href="QML-files/numbered_nodes/"><img src="Example_images/numbered_nodes.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/numbered_nodes/)

### Gauges
<table><tr><td><a href="QML-files/gauges/"><img src="Example_images/gauges.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/gauges/)

### Halftone edge
<table><tr><td><a href="QML-files/halftone_edge/"><img src="Example_images/halftone_edge.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/halftone_edge/)

### Graph charts with labels
<table><tr><td><a href="QML-files/graph_charts_with_labels/"><img src="Example_images/graph_charts_with_labels.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/graph_charts_with_labels/)

### Chaos lines
<table><tr><td><a href="QML-files/chaos_lines/"><img src="Example_images/chaos_lines.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/chaos_lines/)

### Bevelled edge
<table><tr><td><a href="QML-files/bevelled_edge/"><img src="Example_images/bevelled_edge.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/bevelled_edge/)

### Striped edge
<table><tr><td><a href="QML-files/striped_edge/"><img src="Example_images/striped_edge.png" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/striped_edge/)

### Gradient border
<table><tr><td><a href="QML-files/gradient_border/"><img src="Example_images/gradient_border_animated.gif" loading="eager"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/gradient_border/)
